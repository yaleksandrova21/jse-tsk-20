package ru.yaleksandrova.tm.service;

import ru.yaleksandrova.tm.api.repository.IProjectRepository;
import ru.yaleksandrova.tm.api.sevice.IProjectService;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.exception.empty.EmptyIdException;
import ru.yaleksandrova.tm.exception.empty.EmptyIndexException;
import ru.yaleksandrova.tm.exception.empty.EmptyNameException;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;

import java.util.Collections;
import java.util.List;
import java.util.Comparator;


public final class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String userId, final String name) {
        if (name == null || name.isEmpty()) return;
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public Project findByName(final String userId, String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @Override
    public Project updateById(final String userId, String id, String name, String description) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Project project = findById(userId, id);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (index == null || index < 0)
            throw new EmptyIndexException();
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Project project = findByIndex(userId, index);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeByName(String userId, String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.removeByName(userId, name);
    }

    @Override
    public Project startByIndex(String userId, Integer index) {
        if (index == null || index < 0)
            throw new EmptyIndexException();
        return projectRepository.startByIndex(userId, index);
    }

    @Override
    public Project startByName(String userId, String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.startByName(userId, name);
    }

    @Override
    public Project startById(String userId, String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return projectRepository.startById(userId, id);
    }

    @Override
    public Project finishById(String userId, String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return projectRepository.finishById(userId, id);
    }

    @Override
    public Project finishByIndex(String userId, Integer index) {
        if (index == null || index < 0)
            throw new EmptyIndexException();
        return projectRepository.finishByIndex(userId, index);
    }

    @Override
    public Project finishByName(String userId, String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.finishByName(userId, name);
    }

    @Override
    public Project changeStatusById(String userId, String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) return null;
        return projectRepository.changeStatusById(userId, id, status);
    }

    @Override
    public Project changeStatusByIndex(String userId, Integer index, Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) return null;
        return projectRepository.changeStatusByIndex(userId, index, status);
    }

    @Override
    public Project changeStatusByName(String userId, String name, Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) return null;
        return projectRepository.changeStatusByName(userId, name, status);
    }

    @Override
    public Project removeById(String userId, String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        projectRepository.remove(project);
        return project;
    }

    @Override
    public Integer size(String userId) {
        return projectRepository.size();
    }

}
