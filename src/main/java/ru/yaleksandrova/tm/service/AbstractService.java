package ru.yaleksandrova.tm.service;

import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.api.IService;
import ru.yaleksandrova.tm.exception.empty.EmptyIdException;
import ru.yaleksandrova.tm.exception.empty.EmptyIndexException;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.exception.system.IndexIncorrectException;
import ru.yaleksandrova.tm.model.AbstractEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void add(E entity) {
        if (entity == null) throw new ProjectNotFoundException();
        repository.add(entity);
    }

    @Override
    public void remove(E entity) {
        if (entity == null) throw new ProjectNotFoundException();
        repository.remove(entity);
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(comparator);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public int size() {
        return repository.size();
    }

    @Override
    public boolean existsById(String id) {
        if (id == null) throw new EmptyIdException();
        return repository.existsById(id);
    }

    @Override
    public E findById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public E findByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > repository.size()) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Override
    public E removeById(String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public E removeByIndex(String userId, Integer index) {
        if (index == null || index < 0)
            throw new EmptyIndexException();
        return repository.removeByIndex(userId, index);
    }

}
