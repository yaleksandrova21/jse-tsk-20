package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.api.repository.IOwnerRepository;
import ru.yaleksandrova.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public void remove(final String userId, final E entity) {
        final List<E> ownerEntities = findAll(userId);
        ownerEntities.remove(entity);
    }

    @Override
    public void clear(final String userId) {
        final List<E> ownerEntities = findAll(userId);
        ownerEntities.clear();
    }

    @Override
    public List<E> findAll(final String userId) {
        List<E> ownerEntities = new ArrayList<>();
        for (E entity : list) {
            if (userId.equals(entity.getUserId())) ownerEntities.add(entity);
        }
        return ownerEntities;
    }

    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        final List<E> ownerEntities = findAll(userId);
        ownerEntities.sort(comparator);
        return ownerEntities;
    }

    @Override
    public E findById(final String userId, final String id) {
        for(E entity:list){
            if(id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        final List<E> list = findAll(userId);
        return list.get(index);
    }

    @Override
    public E removeById(final String userId, final String id) {
        List<E> ownerEntities = findAll(userId);
        final E entity = findById(userId, id);
        ownerEntities.remove(entity);
        return entity;
    }


    public Integer size(final String userId) {
        final List<E> list = findAll(userId);
        return list.size();
    }

}


