package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected List<E> list = new ArrayList<>();

    @Override
    public void add(final E entity) {
        list.add(entity);
    }

    @Override
    public void remove(final E entity) {
        list.remove(entity);
    }

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        final List<E> entities = new ArrayList<>(list);
        entities.sort(comparator);
        return entities;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean existsById(final String id) {
        return findById(id) != null;
    }

    @Override
    public E findById(final String id) {
        for(E entity:list){
            if(id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final Integer index) {
        return list.get(index);
    }

    @Override
    public E removeById(String id) {
        final E entity = findById(id);
        if (entity == null)
            return null;
        list.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(String userId, final Integer index) {
        final E entity = findByIndex(index);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

}
