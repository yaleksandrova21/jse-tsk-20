package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.api.repository.ITaskRepository;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.AbstractEntity;
import ru.yaleksandrova.tm.model.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public Task findByName(String userId, String name) {
        for(Task task: list){
            if(name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeByName(String userId, String name) {
        final Task task = findByName(userId, name);
        if (task == null)
            return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task startByIndex(String userId, Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByName(String userId, String name) {
        final Task task = findByName(userId, name);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startById(String userId, String id) {
        final Task task = findById(userId, id);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task finishById(String userId, String id) {
        final Task task = findById(userId, id);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByIndex(String userId, Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByName(String userId, String name) {
        final Task task = findByName(userId, name);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task changeStatusById(String userId, String id, Status status) {
        final Task task = findById(userId, id);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(String userId, Integer index, Status status) {
        final Task task = findByIndex(userId, index);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(String userId, String name, Status status) {
        final Task task = findByName(userId, name);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task bindTaskToProjectById(String userId, String projectId, String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(String userId, String projectId) {
        final List<Task> listByProject = new ArrayList<>();
        for (Task task : list) {
            if (projectId.equals(task.getProjectId())) listByProject.add(task);
        }
        if (listByProject.size() > 0) return listByProject;
        else return null;
    }

    @Override
    public Task unbindTaskById(String userId, String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(String userId, String projectId) {
        List<Task> newTasksList = new ArrayList<>();
        for (final Task task: list) {
            if (!projectId.equals(task.getProjectId())) newTasksList.add(task);
        }
        list = newTasksList;
    }

}
