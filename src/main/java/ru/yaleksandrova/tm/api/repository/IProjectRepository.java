package ru.yaleksandrova.tm.api.repository;

import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;
import java.util.List;
import java.util.Comparator;

public interface IProjectRepository extends IOwnerRepository<Project> {

    Project findByName(String userId, String name);

    Project removeByName(String userId, String name);

    Project startByIndex(String userId, Integer index);

    Project startByName(String userId, String name);

    Project startById(String userId, String id);

    Project finishById(String userId, String id);

    Project finishByIndex(String userId, Integer index);

    Project finishByName(String userId, String name);

    Project changeStatusById(String userId, String id, Status status);

    Project changeStatusByIndex(String userId, Integer index, Status status);

    Project changeStatusByName(String userId, String name, Status status);

}
