package ru.yaleksandrova.tm.model;

import ru.yaleksandrova.tm.repository.AbstractRepository;

import java.util.UUID;

public class AbstractEntity<E extends AbstractRepository> {

    protected String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
