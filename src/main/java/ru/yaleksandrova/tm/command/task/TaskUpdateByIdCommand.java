package ru.yaleksandrova.tm.command.task;

import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("ENTER ID:");
        final String id = ApplicationUtil.nextLine();
        final Task task  = serviceLocator.getTaskService().findById(userId, id);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        System.out.println("ENTER NAME:");
        final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = ApplicationUtil.nextLine();
        final Task taskUpdatedId = serviceLocator.getTaskService().updateById(userId, id, name, description);
        if (taskUpdatedId == null) {
            return;
        }
        System.out.println("[Task updated]");
    }

}
