package ru.yaleksandrova.tm.command.task;

import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class TaskChangeStatusByNameCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-change-status-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change task status by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        final String statusValue = ApplicationUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = serviceLocator.getTaskService().changeStatusByName(userId, name, status);
        if (task == null) {
            throw new TaskNotFoundException();
        }
    }

}
