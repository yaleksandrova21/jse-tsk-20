package ru.yaleksandrova.tm.command.user;

import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.command.AbstractUserCommand;
import ru.yaleksandrova.tm.model.User;

public class UserShowProfileCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-show-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show user profile";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[USER PROFILE]");
        showUser(user);
    }

}
