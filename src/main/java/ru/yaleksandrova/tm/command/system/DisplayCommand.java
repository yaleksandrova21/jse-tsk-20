package ru.yaleksandrova.tm.command.system;

import ru.yaleksandrova.tm.command.AbstractCommand;
import java.util.Collection;

public final class DisplayCommand extends AbstractCommand {

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String arg() {
        return "-cmd";
    }

    @Override
    public String description() {
        return "Display list commands";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands)
            System.out.println(command.name());
    }

}
