package ru.yaleksandrova.tm.command.system;

import ru.yaleksandrova.tm.command.AbstractCommand;
import java.util.Collection;

public final class ArgumentsDisplayCommand extends AbstractCommand {

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String description() {
        return "Display list arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand argument : arguments)
            System.out.println(argument.arg());

    }

}
