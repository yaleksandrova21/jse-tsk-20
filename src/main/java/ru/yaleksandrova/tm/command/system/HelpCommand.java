package ru.yaleksandrova.tm.command.system;

import ru.yaleksandrova.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "Display list of commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            System.out.println(command);
        }
    }

}
