package ru.yaleksandrova.tm.command.system;

import ru.yaleksandrova.tm.command.AbstractCommand;

public final class AboutDisplayCommand extends AbstractCommand {

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Display developer information";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Yulia Aleksandrova");
        System.out.println("E-mail: yaleksanrova@yandex.ru");
    }

}
